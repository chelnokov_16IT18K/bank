package ru.chelnokov.bank;

/**
 * Класс с конструкторами и методами для объекта аккаунта банка
 *
 * @author Chelnokov E.I.
 */

class BankAccount {
    private int balance;

    /**
     * Кооструктор объекта аккаунт банка
     * @param balance текущий баланс
     */
    BankAccount(int balance) {
        this.balance = balance;
    }

    /**
     * Метод для внесения денег на счёт
     * @param sum сумма внесения
     */
    synchronized void put(int sum){
        this.balance = balance + sum;
        System.out.println("Ваш баланс увеличился на " + sum + " рублей");
        System.out.println("Теперь ваш баланс: " + balance);
    }

    /**
     * Метод для снятия денег со счёта
     * @param sum сумма снятия
     */
    synchronized void takeOff(int sum){
        this.balance = balance - sum;

    }

    /**
     * Метод для передачи значения баланса
     * @return текущий баланс
     */
    int getBalance() {
        return balance;
    }
}

