package ru.chelnokov.bank;

/**
 * Демокласс для проекта банк с созданием объектов
 *
 * @author Chelnokov E.I.
 */
public class Demo {
    public static void main(String[] args) {
        BankAccount bankAccount = new BankAccount(0);
        PutMoneyThread putMoneyThread = new PutMoneyThread(bankAccount);
        TakeOffThread takeOffThread = new TakeOffThread(bankAccount);
        putMoneyThread.start();
        takeOffThread.start();
    }
}
