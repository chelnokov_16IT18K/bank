package ru.chelnokov.bank;

/**
 * Класс для вненсения денег на счёт
 *
 * @author Chelnokov E.I.
 */

public class PutMoneyThread extends Thread {
    private final BankAccount bankAccount;

    /**
     * Конструктор для объекта-потока для внесения денег
     *  @param bankAccount объект типа BankAccount
     *
     */
    PutMoneyThread(BankAccount bankAccount) {
        this.bankAccount = bankAccount;
    }

    /**
     * Метод запуска потока
     */
    public void run() {
        for (int i = 0; i < 100; i++) {
            synchronized (bankAccount) {
                bankAccount.put(70);
                System.out.println("+70 рублей!");
                System.out.println("Текущий баланс " + bankAccount.getBalance());
                if (bankAccount.getBalance() >= 300) {
                    bankAccount.notifyAll();
                }
            }
        }
    }
}