package ru.chelnokov.bank;

/**
 * Класс для снятия денег со счёта
 *
 * @author Chelnokov E.I.
 */

public class TakeOffThread extends Thread {
    private final BankAccount bankAccount;

    /**
     * Конструктор для объекта-потока, снимающего деньги
     *  @param bankAccount объект типа BankAccount
     *
     */
    TakeOffThread(BankAccount bankAccount) {
        this.bankAccount = bankAccount;
    }

    /**
     * метод запуска потока
     */
    public void run() {
        for (int i = 0; i < 50; i++) {
            synchronized (bankAccount) {
                while (bankAccount.getBalance() < 300) {
                    try {
                        bankAccount.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                bankAccount.takeOff(300);
                System.out.println("-300 рублей!");
                System.out.println("Текущий баланс: " + bankAccount.getBalance());
            }
        }
    }
}

